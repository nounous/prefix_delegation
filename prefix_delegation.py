#!/usr/bin/python3

import argparse
import configparser
import ipaddress
import itertools
import json
import os
import subprocess
import sys

import ldap


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Generate routes for prefix delegation",
    )

    parser.add_argument('-e', '--export', help="Exporte les règles de délégation sur la sortie standard", action='store_true')
    args = parser.parse_args()

    with open(os.path.join(path, 'prefix_delegation.json')) as config_file:
        config = json.load(config_file)

    base = ldap.initialize(config['ldap']['server'])
    if config['ldap']['server'].startswith('ldaps://'):
        # On ne vérifie pas le certificat pour le LDAPS
        base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
    base.simple_bind_s(config['ldap']['binddn'], config['ldap']['password'])

    protocol = config['protocol']
    interface = config['interface']

    insert = subprocess.run(['/bin/systemctl', 'is-active', 'router.target'], stdout=subprocess.DEVNULL).returncode == 0

    current_routes = json.loads(subprocess.run(['/sbin/ip', '--json', '-6', 'route', 'show', 'protocol', protocol], capture_output=True).stdout.decode('ascii'))
    current_routes = { ipaddress.IPv6Network(route['dst']): ipaddress.IPv6Address(route['gateway']) for route in current_routes }

    delegations = {'delegated_prefixes': []}
    networks = base.search_s("ou=networks,dc=adh,dc=crans,dc=org", ldap.SCOPE_ONELEVEL, "objectClass=ipNetwork")
    networks = [{key:[value.decode('utf8') for value in values] for key,values in network.items()}  for dn, network in networks ]
    for network in networks:
    	delegations['delegated_prefixes'].append({"gateway":network['ipHostNumber'][0], "delegated_prefix":network['ipNetworkNumber'][0], "delegated_length":network['ipNetmaskNumber'][0]})


    prefix = ipaddress.IPv6Network(f"{config['prefix']}/{config['length']}")
    delegated_prefixes = {}
    for delegated_prefix in delegations['delegated_prefixes']:
        delegated_prefixes[ipaddress.IPv6Network(f"{delegated_prefix['delegated_prefix']}/{delegated_prefix['delegated_length']}")] = ipaddress.IPv6Address(delegated_prefix['gateway'])
    for prefix_1, prefix_2 in itertools.combinations(delegated_prefixes.keys(), 2):
        if prefix_1.overlaps(prefix_2):
            print("Error: {prefix_1} and {prefix_2} overlap !", file=sys.stderr)
            exit(1)
    for delegated_prefix, gateway in delegated_prefixes.items():
        if delegated_prefix not in current_routes:
            command_add = ['/sbin/ip', 'route', 'add', str(delegated_prefix), 'via', str(gateway), 'dev', interface, 'protocol', protocol]
            if args.export:
                print(' '.join(command_add))
            elif insert:
                subprocess.run(command_add)
        elif gateway != current_routes[delegated_prefix]:
            command_delete = ['/sbin/ip', 'route', 'delete', str(delegated_prefix), 'via', str(current_routes[delegated_prefix]), 'dev', interface, 'protocol', protocol]
            command_add = ['/sbin/ip', 'route', 'add', str(delegated_prefix), 'via', str(gateway), 'dev', interface, 'protocol', protocol]
            if args.export:
                print(' '.join(command_delete))
                print(' '.join(command_add))
            elif insert:
                subprocess.run(command_delete)
                subprocess.run(command_add)
    for delegated_prefix, gateway in current_routes.items():
        if delegated_prefix not in delegated_prefixes:
            command_delete = ['/sbin/ip', 'route', 'delete', str(delegated_prefix), 'via', str(gateway), 'dev', interface, 'protocol', protocol]
            if args.export:
                print(' '.join(command_delete))
            elif insert:
                    subprocess.run(command_delete)
